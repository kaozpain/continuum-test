<?php

namespace Tests\Unit;

use App\Models\Client;
use App\Models\Transaction;
use Faker\Factory;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class GeneralTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Databse Test, and fake seeder
     */
    public function testCreatingRecords() {
        $quantityOfClients = 3;
        $faker = Factory::create();
        for ($cont = 0; $cont < $quantityOfClients; $cont++){
            $Client = new Client();
            $Client->first_name = $faker->firstName;
            $Client->last_name = $faker->lastName;
            $Client->email = $faker->email;
            $Client->avatar = $faker->imageUrl(100, 100);
            $status = $Client->save();
            $this->assertTrue($status, 'An error on Client Database Result');
            $rand = rand(0,20);
            for ($contTrans = 0; $contTrans < $rand; $contTrans++) {
                $Transaction = new Transaction();
                $Transaction->client_id = $Client->id;
                $Transaction->amount = $faker->randomFloat(2);
                $status = $Transaction->save();
                $this->assertTrue($status, 'An error on Transaction Database Result');

            }
        }
    }

    /**
     * Testing the delete of clients and cascade
     *
     * @throws \Exception
     */
    public function testingClientDelete()
    {
        /** @var Client $Client */
        $Client = Client::all()->first();
        $id = $Client->id;
        $transactionsIds = $Client->Transactions()->pluck('id')->toArray();
        $status = $Client->delete();
        $this->assertTrue($status, 'An Error occured while trying to delete the client. Client Id: '. $id);
        $this->assertFalse(DB::table('client')->where(['id' => $id])->exists(), 'Client has not been eliminated. Client Id: '. $id);
        $this->assertFalse(DB::table('transaction')->where(['id' => $transactionsIds])->exists(), 'Clients Transactions has not been eliminated. transactions Ids: '. implode(',', $transactionsIds));
    }

    /**
     * Testing deleting a transaction individually
     */
    public function testingTransactionDelete()
    {
        /** @var Transaction $Client */
        $Transaction = Transaction::all()->first();
        $id = $Transaction->id;
        $status = $Transaction->delete();
        $this->assertTrue($status, 'An Error occured while trying to delete the transaction. Transaction Id: '. $id);
        $this->assertFalse(DB::table('transaction')->where(['id' => $id])->exists(), 'Transaction has not been eliminated. Transaction Id: '. $id);
    }

    /**
     * Testing the full name method
     */
    public function testingClientGetFullName()
    {
        /** @var Client $Client */
        $Client = Client::all()->first();
        $expectedFullName = $Client->first_name. ' ' . $Client->last_name;
        $this->assertEquals($expectedFullName, $Client->getFullName(), 'Expected name is different that the one who is returned by the function');
    }
}
