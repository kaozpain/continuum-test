<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', 'ClientController@index')->name('clientPagination');
Route::post('/delete', 'Controller@deleteObject')->name('delete');

Route::prefix('client')->group(function (){
    Route::get('/create', 'ClientController@create')->name('clientCreation');
    Route::get('/previewAvatar/{id}', 'ClientController@previewAvatar')->name('clientPreviewAvatar');
    Route::post('/store', 'ClientController@store')->name('clientStore');
});

Route::prefix('transaction')->group(function (){
    Route::get('/{clientId}', 'TransactionController@index')->name('transactionPagination');
    Route::get('/create/{clientId}', 'TransactionController@create')->name('transactionCreation');
    Route::post('/store', 'TransactionController@store')->name('transactionStore');
});

Route::get('{object}/edit/{objectId}', function ($object, $objectId) {
    if ($object == 'client')
        return (new \App\Http\Controllers\ClientController())->edit($objectId);
    else if ($object == 'transaction')
        return (new \App\Http\Controllers\TransactionController())->edit($objectId);
});


