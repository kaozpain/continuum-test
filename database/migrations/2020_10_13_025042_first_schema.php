<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FirstSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('client');
        Schema::dropIfExists('transaction');
        $this->createTableClients();
        $this->createTableTransactions();
    }

    public function createTableClients() {
        Schema::create('client', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('avatar');
            $table->string('email', 30)->unique();
        });
    }

    public function createTableTransactions() {
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->timestamp('transaction_date');
            $table->double('amount');
            $table->foreign('client_id')->references('id')->on('client');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
        Schema::dropIfExists('transaction');
    }
}
