<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class adminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdminUser();
    }

    public function createAdminUser() {
        $user = new User();
        $user->password = Hash::make('password');
        $user->email = 'admin@admin.com';
        $user->name = 'The Super User';
        $user->save();
    }
}
