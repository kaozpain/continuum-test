<h1>Keys for putting this project to production. (Linux way)</h1>

<h2>Install php</h2>
1) apt-get install php7.*
2) verify its running

<h2>Install Apache</h2>
1) apt install apache2
2) Go to the apache2.conf and rewrite to "AllowOverride All" in case the project give problems

<h2>Install mysql</h2>
1) apt install mysql-server
2) Configure Msql (Allows graphycal way)
3) Log in to mysql with the user you configure
4) Create a user and give him privilege (I give it super user privilege)
* CREATE USER 'user'@'localhost' IDENTIFIED BY 'password';
* GRANT ALL PRIVILEGES ON * . * TO 'user'@'localhost';

<h2>Clone Project</h2>
1) go to the directory where the server is running (default: var/www/html whit port 80)
2) git clone git@gitlab.com:kaozpain/continuum-test.git
3) enter the project directory and Copy .env.example and rename it to .env
* cp .env.example ../directory/.env
* enter .env and configure the app suffix constants and databse constants
4) php composer install
5) in case the key is not generated, generate yourself
* php artisan key:generate
6) redirect access(.htaccess) of apache and/or the project for not having the nameProject/public

<h2>Migrate schema, and default user</h2>
php artisan migrate
php artisan db:seed --class=adminUser

if permission error, let permission to the directories

Other more easy way is to pull my docker image, that is a debian OS already configure and do the steps from "Clone repository" and link the ip it gives you, to the apache port
