<?php

namespace App\Models;

class Transaction extends GeneralModel
{
    const CREATED_AT = 'transaction_date';
    const UPDATED_AT = null;

    public $table = 'transaction';
    protected $fillable = ['client_id', 'amount'];

    public function Client() {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Gets the Client Id
     *
     * @return mixed
     */
    public function getClientId() {
        return $this->Client->id;
    }
}
