<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\optionable;
use App\Http\Requests\StoreClientPost;
use App\Models\Client;
use App\Models\Transaction;
use App\Rules\emailUsed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Validator;
use phpDocumentor\Reflection\Types\Parent_;

class ClientController extends Controller
    implements optionable
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->options = $this->getOptions();
    }

    /**
     * Gets options for functionally views
     *
     * @return array
     */
    public function getOptions(){
        return [
            'title' => 'Clients',
            'addNew' => true,
            'row' => [
                'action' => [
                    'delete' => true,
                    'edit' => true,
                    'previewAvatar' => true,
                    'seeTransactions' => true,
                ]
            ]
        ];
    }

    /**
     * Main View
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $data['title'] = $this->options['title'];
        $data['Controller'] = $this;
        return view('client.index', $data);
    }

    /**
     * Principal Page with list of Client paginated by 10
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pagination() {
        $data['options'] = $this->options;
        $Clients = DB::table('client')->paginate(10);
        $data['Clients'] = $Clients;
        $data['class'] = Client::class;
        return view('client.pagination', $data);
    }

    /**
     * Edit the Client Record
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {
        $data['Client'] = Client::find($id);
        return view('client.recordForm', $data);
    }

    /**
     * Gets the Client create view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('client.recordForm');
    }

    /**
     * Creates or update a Client registry
     *
     * @param StoreClientPost $Request
     * @return bool
     */
    public function store(Request $Request)
    {
        $required = $Request->id ? '' : 'required|';
        $validator = \Illuminate\Support\Facades\Validator::make($Request->all(), [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email|max:30|unique:client,email,'.$Request->id,
            'avatar' => $required.'image|mimes:jpeg,png,jpg,gif,svg|dimensions:width=100,height=100',
        ]);

        if ($validator->fails())
        {
            if ($Request->id)
                return redirect('client/edit/'.$Request->id)->withErrors($validator);
            else
                return redirect('client/create')->withErrors($validator);
        }

        if ($Request->file('avatar')) {
            $imagePath = $Request->file('avatar');
            $extension = $imagePath->extension();
            $FileName = str_replace('.', '_',$Request->email);
            $FileName = str_replace('@', '-',$FileName).'.'.$extension;

            $path = 'storage/'.$Request->file('avatar')->storeAs('uploads', $FileName, 'public');
        }

        /** @var Client $Client */
        if ($clientId = $Request->id) {
            $Client = Client::find($clientId);
        } else {
            $Client = new Client();
        }

        $Client->first_name = $Request->first_name;
        $Client->last_name = $Request->last_name;
        $Client->email = $Request->email;
        $Client->avatar = $Request->file('avatar') ? $path : $Client->avatar;
        $Client->save();

        return redirect('/');
    }

    /**
     * Previews the avatar send by the client
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function previewAvatar($id) {
        /** @var Client $Client */
        $Client = Client::find($id);
        $data['avatarUrl'] = asset($Client->avatar);

        return view('client.previewAvatar', $data);
    }

}
