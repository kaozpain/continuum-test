<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Deletes the record physically
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteObject() {
        /** @var Model $Object */
        $Object = $_POST["class"]::find($_POST['class_id']);
        $status = $Object->delete();

        return Response::json(['success' => $status], 200);
    }
}
