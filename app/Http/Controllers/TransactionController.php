<?php

namespace App\Http\Controllers;

use App\Http\Interfaces\optionable;
use App\Models\Client;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
    implements optionable
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->options = $this->getOptions();
    }

    /**
     * Gets options for functionally views
     *
     * @return array
     */
    public function getOptions(){
        return [
            'title' => 'Transactions',
            'addNew' => true,
            'row' => [
                'action' => [
                    'delete' => true,
                    'edit' => true,
                    'previewAvatar' => false,
                    'seeTransactions' => false,
                ]
            ]
        ];
    }

    /**
     * Main View of Client Transactions
     *
     * @param $clientId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($clientId) {
        $data['title'] = $this->options['title'];
        $data['Controller'] = $this;
        /** @var Client $Client */
        $Client = Client::find($clientId);
        $data['client_full_name'] = $Client->getFullName();
        $data['client_id'] = $clientId;
        return view('transaction.index', $data);
    }

    /**
     * Principal Page with list of Transactions paginated by 10
     *
     * @param $clientId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pagination($clientId) {
        $data['options'] = $this->options;
        /** @var Client $Client */
        $Transactions = DB::table('transaction')->where(['client_id' => $clientId])->paginate(10);
        $data['Transactions'] = $Transactions;
        $data['class'] = Transaction::class;
        $data['client_id'] = $clientId;
        return view('transaction.pagination', $data);
    }

    /**
     * Gets the Transaction create view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($clientId)
    {
        $data['client_id'] = $clientId;
        return view('transaction.recordForm', $data);
    }

    /**
     * Edit the Transaction Record
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($transactionId) {
        /** @var Transaction $Transaction */
        $Transaction = Transaction::find($transactionId);
        $data['Transaction'] = $Transaction;
        $data['client_id'] = $Transaction->getClientId();
        return view('transaction.recordForm', $data);
    }

    /**
     * Creates or update a Client registry
     *
     * @param StoreClientPost $Request
     * @return bool
     */
    public function store(Request $Request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($Request->all(), [
            'amount' => "required|regex:/^\d+(\.\d{1,2})?$/"
        ]);

        if ($validator->fails())
        {
            if ($Request->id)
                return redirect('transaction/edit/'.$Request->id)->withErrors($validator);
            else
                return redirect('transaction/create/'.$Request->client_id)->withErrors($validator);
        }

        if ($transactionId = $Request->id) {
            $Transaction = Transaction::find($transactionId);
        } else {
            /** @var Client $Client */
            $Transaction = new Transaction();
            $Transaction->client_id = $Request->client_id;
        }

        $Transaction->amount = $Request->amount;
        $Transaction->save();

        return redirect('/transaction/'.$Request->client_id);
    }
}
