<?php

namespace App\Http\Interfaces;


interface optionable
{

    public function getOptions();
}
