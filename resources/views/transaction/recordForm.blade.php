@extends('layouts.app')

@section('content')
    <div class="card container">
        <div class="card-title text-center pt-3">
            <h1>Client</h1>
        </div>
        <div class="card-body">
            <div class="alert alert-danger" style="display:none"></div>
            <form method="post" action="{{ route('transactionStore') }}" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <div class="col-12">
                        <div class="form-group">
                            <input name="client_id" type="hidden" value="{{ $client_id }}">
                            @isset($Transaction)
                                <input name="id" type="hidden" value="{{ $Transaction->id }}">
                            @endisset
                            <div class="col-6 float-left">
                                <label for="amount" class="font-weight-bold">Amount:</label>
                                <input value="@isset($Transaction) {{ $Transaction->amount }} @endisset" type="text" min="0.01" step="0.01" class="form-control" name="amount"  id="amount" placeholder="amount">
                                @if($errors->get('amount'))
                                    <div class="alert alert-danger">
                                        {{ $errors->get('amount')[0] }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-6 float-left">
                                <label for="date" class="font-weight-bold">Date:</label>
                                <input disabled value="@isset($Transaction) {{ $Transaction->transaction_date }} @else {{ date("Y/m/d") }} @endisset" type="text" class="form-control" name="date" id="date">
                                @if($errors->get('last_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->get('last_name')[0] }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="text-center m-5">
                    <a class="text-right col-5"><button type="submit" class="btn btn-success h-25 w-25 align-top">Save</button></a>
                    <a href="{{ url('transaction/'.$client_id) }}" class="text-left col-5"><p class="btn btn-dark h-25 w-25">Exit</p></a>
                </div>
            </form>
        </div>
    </div>
@endsection
