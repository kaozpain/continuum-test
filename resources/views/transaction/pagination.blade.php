<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Amount</th>
            <th scope="col">Date Created</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($Transactions as $Transaction)
            <tr>
                <td> <strong>{{ html_entity_decode('&pound;') }} </strong>{{ $Transaction->amount }}</td>
                <td> {{ $Transaction->transaction_date }}</td>
                <td>
                    <action-component :object="{{ json_encode(['class' => $class , 'class_id' => $Transaction->id]) }}"
                                      :menu-actions="{{ json_encode($options['row']) }}" route="{{ url('transaction/'.$client_id) }}"></action-component>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{ $Transactions->links() }}
