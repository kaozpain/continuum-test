@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="card container">
            <a href="{{ route('clientPagination') }}"><button type="button" class="">Back</button></a>
            <h1 class="card-title align-self-center font-weight-bold">{{ $title }}</h1>
            <h2><strong>Client Name:</strong> {{ $client_full_name }}</h2>
            <div class="text-right m-2">
                <a href="{{ url('transaction/create/'.$client_id) }}"><button type="button" class="btn btn-success col-2 border-0">add</button></a>
            </div>
            <div id="transaction_pagination">
                {{ $Controller->pagination($client_id) }}
            </div>
        </div>
        </div>
    </div>
@endsection
