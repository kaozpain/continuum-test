<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">E-mail</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
    @foreach ($Clients as $Client)
        <tr>
            <td> {{ $Client->first_name }}</td>
            <td> {{ $Client->last_name }}</td>
            <td> {{ $Client->email }}</td>
            <td>
                <action-component :object="{{ json_encode(['class' => $class , 'class_id' => $Client->id]) }}"
                                  :menu-actions="{{ json_encode($options['row']) }}" route="{{ route('clientPagination') }}"></action-component>
            </td>
        </tr>
    @endforeach
        </tbody>
    </table>
</div>

{{ $Clients->links() }}
