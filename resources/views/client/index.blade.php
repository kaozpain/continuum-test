@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="card container">
            <h1 class="card-title align-self-center font-weight-bold">{{ $title }}</h1>
            <div class="text-right m-2">
                <a href="{{ route('clientCreation') }}"><button type="button" class="btn btn-success col-2 border-0">add</button></a>
                {{--<button type="button" class="btn btn-success col-2 border-0" onclick="custom.client.create()">add</button>--}}
            </div>
            <div id="client_pagination">
                {{ $Controller->pagination() }}
            </div>
        </div>
        <div class="modal fade" id="client_modal" tabindex="-1" role="dialog" aria-hidden="true">
        </div>
    </div>
@endsection
