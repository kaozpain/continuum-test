@extends('layouts.app')

@section('content')
    <div class="card container">
        <div class="card-title text-center pt-3">
            <h1>Client</h1>
        </div>
        <div class="card-body">
            <div class="alert alert-danger" style="display:none"></div>
            <form method="post" action="{{ route('clientStore') }}" enctype="multipart/form-data">
                @csrf
                <fieldset>
                    <div class="col-12">
                        <div class="form-group">
                            @isset($Client)
                                <input name="id" type="hidden" value="{{ $Client->id }}">
                            @endisset
                            <div class="col-6 float-left">
                                <label for="first_name" class="font-weight-bold">First name:</label>
                                <input value="@isset($Client) {{ $Client->first_name }} @endisset" type="text" class="form-control" name="first_name"  id="first_name" placeholder="First name">
                                @if($errors->get('first_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->get('first_name')[0] }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-6 float-left">
                                <label for="last_name" class="font-weight-bold">Last name:</label>
                                <input value="@isset($Client) {{ $Client->last_name }} @endisset" type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name">
                                @if($errors->get('last_name'))
                                    <div class="alert alert-danger">
                                        {{ $errors->get('last_name')[0] }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-12 float-left">
                                <label for="email" class="font-weight-bold">E-mail:</label>
                                <input value="@isset($Client) {{ $Client->email }} @endisset" type="text" class="form-control" name="email" id="email" placeholder="E-mail">
                                @if($errors->get('email'))
                                    <div class="alert alert-danger">
                                        {{ $errors->get('email')[0] }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-12 float-left">
                                <label for="avatar" class="font-weight-bold">Avatar: (100x100)</label>
                                <input value="" type="file" class="form-control" name="avatar" id="avatar" onchange="custom.client.previewNewAvatar(event)">
                                @if($errors->get('avatar'))
                                    <div class="alert alert-danger">
                                        {{ $errors->get('avatar')[0] }}
                                    </div>
                                @endif
                                <div class="row m-2">
                                    @isset($Client)
                                    <div class="col-3 float-left">
                                        <p>Current Avatar: </p>
                                            <img src="{{ asset($Client->avatar) }}">
                                    </div>
                                    @endisset
                                    <div class="col-3" id="new_avatar" style="display: none">
                                        <p>New Avatar: </p>
                                        <img class="formImages" id="new_avatar_img" alt="new Avatar" src="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="text-center m-5">
                    <a class="text-right col-5"><button type="submit" class="btn btn-success h-25 w-25 align-top">Save</button></a>
                    <a href="{{ route('clientPagination') }}" class="text-left col-5"><p class="btn btn-dark h-25 w-25">Exit</p></a>
                </div>
            </form>
        </div>
    </div>
@endsection
