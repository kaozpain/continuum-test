<div class="modal-dialog mx-auto" role="document">
    <div class="modal-content">
        <button type="button" class="close text-black-50" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="m-1 text-center">
            <img src="{{ $avatarUrl }}" alt="Error on Reading Image">
        </div>
    </div>
</div>
