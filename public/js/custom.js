let custom = {
    client: {
        previewNewAvatar: function (event) {
            $('#new_avatar').show();
            console.log($('#new_avatar_img'));
            $('#new_avatar_img')[0].src = URL.createObjectURL(event.target.files[0]);
        },
    },
};
